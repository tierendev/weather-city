//
//  File.swift
//  Weather City
//
//  Created by Sothearith Sreang on 21/10/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import Foundation

enum GenericError : Error {
    case runtimeError(String)
    case nullError(String)
}

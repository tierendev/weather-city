//
//  WeatherTableViewCell.swift
//  Weather City
//
//  Created by Sothearith Sreang on 6/11/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {
    
    static let CELL_IDENTIFIER = "WeatherTableViewCell"

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var summary: UILabel!
    
    func populate(_ weather: Weather) {
        icon.image = weather.icon.image()
        let temp = Int(weather.temperature())
        temperature.text = "\(temp) °C"
        date.text = weather.date()
        summary.text = weather.summary
    }
}

extension WeatherIcon {
    func image() -> UIImage? {
        let iconName = self.rawValue
        guard let image = UIImage.init(named: iconName) else {
            print("cannot find image: \(iconName)")
            return nil
        }
        
        return image
    }
}

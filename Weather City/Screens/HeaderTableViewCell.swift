//
//  HeaderTableViewCell.swift
//  Weather City
//
//  Created by Sothearith Sreang on 8/11/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import UIKit

class HeaderTableViewCell: UITableViewCell {
    
    static let CELL_IDENTIFIER = "HeaderTableViewCell"

    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var summary: UILabel!
    
    func populate(_ forecast: Forecast) {
        location.text = forecast.timezone
        summary.text = forecast.daily.summary
    }
    
}

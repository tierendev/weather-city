//
//  ViewController.swift
//  Weather City
//
//  Created by Sothearith Sreang on 27/9/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    private let disposables = DisposeBag()
    private var forecast: Forecast? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        initTableView()
        initSearchBar()
    }
    
    private func initTableView() {
        tableView.tableFooterView = UIView()
    }
    
    private func initSearchBar() {
        let searchStream = searchBar.rx.text
            .orEmpty
            .debounce(0.3, scheduler: MainScheduler.instance)
            .map { $0 == "" ? "Singapore" : $0 }
        
        let locationStream = searchStream.flatMap { location in
            return getWeeklyForecasts(location: location)
        }
        
        return locationStream
            .observeOn(MainScheduler.instance)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(
                onNext: { forecast in
                    self.forecast = forecast
                    self.tableView.reloadData()
                },
                onError: { error in
                    print("error", error)
                }
            )
            .disposed(by: disposables)
    }

}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = forecast?.daily.data.count ?? 0
        return count == 0 ? 0 : (count + 1)
    }
    
    // TODO: Configure row height on Storyboard
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 85
        } else {
            return 64
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let indexRow = indexPath.row
        if indexRow == 0 {
            return populateHeader(tableView)
        } else {
            return populateWeatherCell(tableView, indexRow: indexRow - 1)
        }
    }
    
    private func populateHeader(_ tableView: UITableView) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: HeaderTableViewCell.CELL_IDENTIFIER) as! HeaderTableViewCell
        if let forecast = self.forecast {
            cell.populate(forecast)
        }
        
        return cell
    }
    
    private func populateWeatherCell(_ tableView: UITableView, indexRow: Int) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: WeatherTableViewCell.CELL_IDENTIFIER) as! WeatherTableViewCell
        if let weather = forecast?.daily.data[indexRow] {
            cell.populate(weather)
        }
        
        return cell
    }
}


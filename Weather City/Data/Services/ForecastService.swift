//
//  ForecastService.swift
//  Weather City
//
//  Created by Sothearith Sreang on 29/9/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import Foundation
import CoreLocation
import RxSwift

private struct Url {
    static let API_KEY = "f54f210d5bf747ef175f54ad070eccc7"
    static let DARK_SKY_BASE = "https://api.darksky.net"
    static let FORECAST_ENDPOINT = "/forecast"
    static let EXCLUDE_PARAMS = "currently,flags,hourly"
}

struct Header {
    static let CONTENT_TYPE = "Content-Type"
    static let CONTENT_TYPE_JSON = "application/json"
}

private func getGeoLocation(_ location: String) -> Single<CLLocation> {
    return Single.create { (observer) in
        CLGeocoder().geocodeAddressString(location) { (placemark: [CLPlacemark]?, error: Error?) in
            if let geoCodeError = error {
                observer(.error(geoCodeError))
            } else if let geoLocation = placemark?.first?.location {
                observer(.success(geoLocation))
            }
        }
        return Disposables.create()
    }
}

private func getWeeklyForecastUrl(_ latitude: Double, _ longitude: Double) -> String {
    return Url.DARK_SKY_BASE
            + Url.FORECAST_ENDPOINT + "/" + Url.API_KEY + "/"
            + String(latitude) + "," + String(longitude)
            + "?units=si"
            + "&exclude=" + Url.EXCLUDE_PARAMS
}

private func getWeeklyForecastsRequest(_ latitude: Double, _ longitude: Double) -> Single<Forecast> {
    return Single.create { (observer) in
        let urlString = getWeeklyForecastUrl(latitude, longitude)
        if let url = URL(string: urlString) {
            var request = URLRequest.init(url: url)
            request.addValue(Header.CONTENT_TYPE_JSON, forHTTPHeaderField: Header.CONTENT_TYPE)
            
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                if let responseError = error {
                    observer(.error(responseError))
                } else if let data = data {
                    guard let forecastResponse = try? JSONDecoder().decode(Forecast.self, from: data) else {
                        observer(.error(GenericError.runtimeError("cannot parse WeatherForecast")))
                        return
                    }
                    
                    observer(.success(forecastResponse))
                }
            }
            
            task.resume()
            
        } else {
            observer(.error(GenericError.nullError("cannot init url \(urlString)")))
        }
        
         return Disposables.create()
    }
}

func getWeeklyForecasts(location: String) -> Single<Forecast> {
    return getGeoLocation(location)
        .flatMap { (location: CLLocation) in
            return getWeeklyForecastsRequest(location.coordinate.latitude, location.coordinate.longitude)
        }
}


//
//  DailyForecast.swift
//  Weather City
//
//  Created by Sothearith Sreang on 29/9/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import Foundation

struct DailyForecast : Decodable {
    let summary: String
    let icon: WeatherIcon
    let data: [Weather]
}

//
//  Weather.swift
//  Weather City
//
//  Created by Sothearith Sreang on 29/9/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import Foundation

struct Weather : Decodable {
    let time: CLongLong
    let summary: String
    let icon: WeatherIcon
    let humidity: Float
    let pressure: Float
    let temperatureMin: Float
    let temperatureMax: Float
    
    func temperature() -> Float {
        return (temperatureMax + temperatureMin) / 2.0
    }
    
    func date() -> String {
        let timeInterval: TimeInterval = Double(time)
        let date = Date(timeIntervalSince1970: timeInterval)
        return date.toString(format: Date.DDMMMYY_FORMAT) ?? ""
    }
}

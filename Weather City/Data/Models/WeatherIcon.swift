//
//  WeatherIcon.swift
//  Weather City
//
//  Created by Sothearith Sreang on 6/11/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import Foundation

enum WeatherIcon : String, Decodable {
    case ClearDay = "clear-day"
    case ClearNight = "clear-night"
    case Rain = "rain"
    case Snow = "snow"
    case Sleet = "sleet"
    case Wind = "wind"
    case Fog = "fog"
    case Cloudy = "cloudy"
    case PartlyCloudyDay = "partly-cloudy-day"
    case PartyCloudyNight = "partly-cloudy-night"
    
    init(from decoder: Decoder) throws {
        let label = try decoder.singleValueContainer().decode(String.self)
        self = WeatherIcon(rawValue: label) ?? .ClearDay
    }
}

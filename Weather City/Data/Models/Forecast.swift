//
//  Forecast.swift
//  Weather City
//
//  Created by Sothearith Sreang on 29/9/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import Foundation

struct Forecast : Decodable {
    let latitude: Float
    let longitude: Float
    let timezone: String
    let daily: DailyForecast
}
